/*
SQLyog Ultimate v11.24 (64 bit)
MySQL - 5.5.45 : Database - netspeed
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`netspeed` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `netspeed`;

/*Table structure for table `t_device` */

DROP TABLE IF EXISTS `t_device`;

CREATE TABLE `t_device` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '表主键，也作为用户ID',
  `os_name` varchar(30) DEFAULT NULL COMMENT '操作系统名称',
  `os_lang` varchar(30) DEFAULT NULL COMMENT '操作系统语言',
  `os_ver` varchar(20) DEFAULT NULL COMMENT '操作系统版本',
  `app_bundle_id` varchar(100) DEFAULT NULL COMMENT '应用包名',
  `app_version_number` int(3) DEFAULT NULL COMMENT '应用版本号',
  `dev_uuid` varchar(16) DEFAULT NULL COMMENT '设备唯一标识',
  `dev_mac_addr` varchar(16) DEFAULT NULL COMMENT '设备Mac地址',
  `dev_manufacturer` varchar(20) DEFAULT NULL COMMENT '设备制造商',
  `dev_model` varchar(30) DEFAULT NULL COMMENT '设备型号',
  `activated_at` varchar(19) DEFAULT NULL COMMENT '激活时间',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `dev_uuid_2` (`dev_uuid`),
  KEY `dev_uuid` (`dev_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_device` */

insert  into `t_device`(`uid`,`os_name`,`os_lang`,`os_ver`,`app_bundle_id`,`app_version_number`,`dev_uuid`,`dev_mac_addr`,`dev_manufacturer`,`dev_model`,`activated_at`) values (1,'iOS','zh_CN','9.2.1','com.xtools.speedtest',1,'875e918f01de7d4e','94DBC9621AF5','Apple','iPhone6 plus','2016-03-13 08:07:32');

/*Table structure for table `t_speed` */

DROP TABLE IF EXISTS `t_speed`;

CREATE TABLE `t_speed` (
  `sid` int(11) NOT NULL AUTO_INCREMENT COMMENT '表主键',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `ping` int(11) DEFAULT NULL COMMENT '网络延迟',
  `download` int(11) DEFAULT NULL COMMENT '下载速度',
  `upload` int(11) DEFAULT NULL COMMENT '上传速度',
  `bytesForUpload` int(11) DEFAULT NULL COMMENT '上传字节数',
  `bytesForDownload` int(11) DEFAULT NULL COMMENT '下载字节数',
  `networkType` int(11) DEFAULT NULL COMMENT '网络类型：0，unknown；1，cell；2，WiFi',
  `latitude` double DEFAULT NULL COMMENT '经度',
  `longitude` double DEFAULT NULL COMMENT '纬度',
  `testDate` varchar(255) DEFAULT NULL COMMENT '测试时间',
  `crarrierName` varchar(255) DEFAULT NULL COMMENT '运营商',
  `wifiSSID` varchar(255) DEFAULT NULL COMMENT 'WiFi名称',
  `externalIP` varchar(255) DEFAULT NULL COMMENT '外网IP',
  `internalIP` varchar(255) DEFAULT NULL COMMENT '内网IP',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`sid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `t_speed` */

insert  into `t_speed`(`sid`,`uid`,`ping`,`download`,`upload`,`bytesForUpload`,`bytesForDownload`,`networkType`,`latitude`,`longitude`,`testDate`,`crarrierName`,`wifiSSID`,`externalIP`,`internalIP`,`remark`) values (1,6,50,1000000,3000000,10000000,10000000,2,100.12,100.11,'2016-02-28 07:59:41','中国移动','zhuzige','10.11.12.11','192.168.1.1',NULL),(2,4,60,2000000,5000000,20000000,13000000,2,107.12,120.11,'2016-02-28 07:59:41','中国联通','zhuzige','10.11.12.11','192.168.1.1',NULL),(3,3,60,2000000,4000000,25000000,15000000,2,107.12,120.11,'2016-02-28 07:59:41','中国联通','zhuzige','10.11.12.11','192.168.1.1',NULL),(4,3,45,3000000,6000000,15000000,13000000,2,107.12,120.11,'2016-02-28 07:59:41','中国联通','zhuzige','10.11.12.11','192.168.1.1',NULL),(5,2,60,2000000,5000000,20000000,13000000,2,107.12,120.11,'2016-02-28 07:59:41','中国联通','zhuzige','10.11.12.11','192.168.1.1',NULL),(6,1,60,2000000,5000000,20000000,13000000,2,107.12,120.11,'2016-02-28 07:59:41','中国联通','zhuzige','10.11.12.11','192.168.1.1',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `sex` smallint(6) NOT NULL,
  `num` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
