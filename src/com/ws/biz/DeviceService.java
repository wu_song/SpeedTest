package com.ws.biz;

import com.ws.entity.Device;

public interface DeviceService {
	public String activateDevice(Device device);
}
