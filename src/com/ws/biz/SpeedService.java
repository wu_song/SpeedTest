package com.ws.biz;

import java.util.List;

import com.ws.dao.SpeedDao;
import com.ws.entity.Speed;

public interface SpeedService {
	public SpeedDao getSpeedDao();
	
	public String saveSpeed(Speed speed);
	
	
	public List<Speed> getNearByRank(Speed speed);
	
	public List<Speed> getSpeedRankList(int currPage,int rows);
}
