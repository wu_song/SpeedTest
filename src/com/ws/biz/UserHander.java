package com.ws.biz;

import com.ws.dao.UserDAO;
import com.ws.entity.User;

/**
 * @author wusong
 * @Email wu_song4232@126.com
 * 
 */
public interface UserHander {
	public UserDAO getUserDAO();
	
	public User queryByName(String name,String pass); 
}
