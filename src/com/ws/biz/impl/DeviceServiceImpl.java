package com.ws.biz.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ws.biz.DeviceService;
import com.ws.dao.DeviceDao;
import com.ws.entity.Device;
import com.ws.tool.DateUtils;
import com.ws.tool.JSONUtils;

public class DeviceServiceImpl implements DeviceService {
	private DeviceDao deviceDao;
	
	public DeviceDao getDeviceDao() {
		return deviceDao;
	}

	public void setDeviceDao(DeviceDao deviceDao) {
		this.deviceDao = deviceDao;
	}


	@Override
	public String activateDevice(Device device) {
		device.setActivated_at(DateUtils.getCurrDateStr());
		Device device2 = deviceDao.saveDevice(device);
		Integer uid = device2.getUid();
		String activated_at = device2.getActivated_at();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("uid", uid);
		map.put("activated_at", activated_at);
		String returnJson = JSONUtils.mapToJson(map);
		//String returnJson = "{\"uid\":"+uid+",\"activated_at\":\""+activated_at+"\"}";
		return returnJson;
	}

}
