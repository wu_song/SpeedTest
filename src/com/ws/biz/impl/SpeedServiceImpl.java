package com.ws.biz.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.ws.biz.SpeedService;
import com.ws.dao.SpeedDao;
import com.ws.entity.Location;
import com.ws.entity.Speed;
import com.ws.tool.BaiDuMap;

public class SpeedServiceImpl implements SpeedService {
	private SpeedDao speedDao;

	public SpeedDao getSpeedDao() {
		return speedDao;
	}

	public void setSpeedDao(SpeedDao speedDao) {
		this.speedDao = speedDao;
	}

	public String saveSpeed(Speed speed) {
		speedDao.saveSpeed(speed);
		List<Speed> speeds = speedDao.getSpeedList();
		List<Speed> newList = ranking(speeds);
		int sid = speed.getSid();
		String returnJson = "";
		for(Speed speed1 : newList){
			if(sid == speed1.getSid()){
				return returnJson = "{\"rank:\":"+speed1.getRank()+"}";
			}
		}
		return returnJson;
	}

	@Override
	public List<Speed> getSpeedRankList(int currPage, int rows) {
		List<Speed> oldList = speedDao.getSpeedPage(currPage, rows);
		return ranking(oldList);
	}
	private List<Speed> ranking(List<Speed> speeds){
		//按照下载速度排名
		for(Speed speed : speeds){
			Integer oldDownload = speed.getDownload();
			int count = 0;
			for(Speed speed2 : speeds){
				Integer newDownload = speed2.getDownload();
				if(newDownload > oldDownload){
					count++;
				}
			}
			Location location = new Location();
			location.setLon(speed.getLon());
			location.setLat(speed.getLat());
			speed.setLocation(location);
			speed.setRank(1+count++);
//			/**不需要的数据设为null*/
//			speed.setLon(null);
//			speed.setLat(null);
//			speed.setBytesForDownload(null);
//			speed.setBytesForUpload(null);
//			speed.setCrarrierName(null);
//			speed.setExternalIP(null);
//			speed.setInternalIP(null);
//			speed.setRemark(null);
//			speed.setSid(null);
//			speed.setUid(null);
//			speed.setWifiSSID(null);
		}
		return speeds;
	}

	@Override
	public List<Speed> getNearByRank(Speed speed) {
		//从数据库里查出所有的速度数据
		List<Speed> allSpeeds = speedDao.getSpeedList();
		double nowUserLon = speed.getLon() == null ? 0.0 : speed.getLon() ;//当前用户位置的经度
		double nowUserLat = speed.getLat() == null ? 0.0 : speed.getLat() ;//当前用户位置的纬度
		Map<Integer,Double> distanceMap = new HashMap<Integer, Double>();
		//List<Double> distances = new ArrayList<Double>();
		for(Speed oneSpeed : allSpeeds){
			Integer key = oneSpeed.getSid();
			double oneLon = oneSpeed.getLon() == null ? 0.0 : oneSpeed.getLon();
			double oneLat = oneSpeed.getLat() == null ? 0.0 : oneSpeed.getLat();
			double distance = BaiDuMap.GetShortDistance(nowUserLon, nowUserLat, oneLon, oneLat);
			distanceMap.put(key, distance);
			//distances.add(distance);
		}
		Map<Integer,Integer> allRankMap = new HashMap<Integer, Integer>();
		//将得到的距离进行排名(Map里的key是速度表的主键，value是排名的名次)
		for(Integer oldKey : distanceMap.keySet()){
			int count = 0;
			for(Integer newKey : distanceMap.keySet()){
				if(distanceMap.get(newKey) > distanceMap.get(oldKey)){
					count++;
				}
			}
			allRankMap.put(oldKey, count+1);
		}
		Map<Integer,Integer> rankMap = new HashMap<Integer, Integer>();
		//遍历map取距离排名前20的（多个记录可以是一样的名次）
		for(Integer key : allRankMap.keySet()){
			if(allRankMap.get(key) <= 20){
				rankMap.put(key, allRankMap.get(key));
			}
		}
		List<Speed> returnSpeeds = new ArrayList<Speed>();
		for(Integer key : rankMap.keySet()){
			Speed speed2 = speedDao.getSpeedById(key);
			speed2.setRank(rankMap.get(key));
			Location location = new Location();
			location.setLon(speed2.getLon());
			location.setLat(speed2.getLat());
			speed2.setLocation(location);
			speed2.setLon(null);
			speed2.setLat(null);
			speed2.setCrarrierName(null);
			speed2.setBytesForDownload(null);
			speed2.setExternalIP(null);
			speed2.setInternalIP(null);
			speed2.setRemark(null);
			speed2.setWifiSSID(null);
			speed2.setSid(null);
			speed2.setUid(null);
			speed2.setBytesForUpload(null);
			returnSpeeds.add(speed2);
		}
		return returnSpeeds;

	}
}
