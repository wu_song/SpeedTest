package com.ws.biz.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ws.biz.UserHander;
import com.ws.dao.UserDAO;
import com.ws.entity.User;

/**
 * @author wusong
 * @Email wu_song4232@126.com
 * 
 */
public class UserHanderImpl implements UserHander{
	private UserDAO userDAO;

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public User queryByName(String name, String pass) {
		HibernateTemplate ht = userDAO.getHibernateTemplate();
		SessionFactory factory = ht.getSessionFactory();
		Session session = factory.openSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("name", name));
		criteria.add(Restrictions.eq("password", pass));
		List<User> list = criteria.list();
		if (list.size() > 0) {
			session.close();
			//releaseSession(session);
			return list.get(0);
		} else {
			session.close();
		}
		return null;
	}

}
