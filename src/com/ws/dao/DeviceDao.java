package com.ws.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.ws.entity.Device;

public class DeviceDao extends HibernateDaoSupport {
	private static final Logger log = LoggerFactory.getLogger(DeviceDao.class);
	public Device saveDevice(Device device){
		try {
			getHibernateTemplate().save(device);
		} catch (DataAccessException e) {
			e.printStackTrace();
			log.error("Saving instance Of Device failed !");
			throw e;
		}
		return device;
	}
}
