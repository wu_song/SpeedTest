package com.ws.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.ws.entity.Speed;
import com.ws.tool.HibernateUtils;

public class SpeedDao extends HibernateDaoSupport{
	private static final Logger logger = LoggerFactory.getLogger(SpeedDao.class);
	
	public void saveSpeed(Speed speed){
		try {
			getHibernateTemplate().save(speed);
			logger.debug("save successful");
		} catch (DataAccessException e) {
			e.printStackTrace();
			logger.error("Saving instance Of Speed failed !");
			throw e;
		}
	}
	
	public Speed getSpeedById(Integer sId){
		Speed speed = null;
		try {
			speed = getHibernateTemplate().get(Speed.class, sId);
			logger.debug("get instance Of Speed by id successful");
		} catch (DataAccessException e) {
			e.printStackTrace();
			logger.error("get instance Of Speed by id failed !");
			throw e;
		}
		return speed;
	}
	public List<Speed> getSpeedPage(int currPage,int rowsPerPage){
		List<Speed> list = null;
		try {
			Session session = HibernateUtils.getSession();  
			Query query = session.createQuery("from Speed s order by s.download desc ");  
			query.setMaxResults(rowsPerPage); // 每页最多显示几条  
			query.setFirstResult((currPage - 1) * rowsPerPage); // 每页从第几条记录开始  
			list = query.list();  
			session.close();  
		} catch (HibernateException e) {
			e.printStackTrace();
		}  
		return list;
	}
	/**
	 * 
	 * @return 获取所有的速度数据list，并按照下载速度倒序排列
	 */
	public List<Speed> getSpeedList(){
		Session session = HibernateUtils.getSession();  
        Query query = session.createQuery("from Speed s order by s.download desc");  
        List<Speed> list = query.list();  
        session.close();  
        return list;  
	}
}
