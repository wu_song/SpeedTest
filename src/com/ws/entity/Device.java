package com.ws.entity;

public class Device {
	private Integer uid;//用户ID
	private String os_name;//操作系统名称
	private String os_lang;//操作系统语言
	private String os_ver;//操作系统版本
	private String app_bundle_id;//应用包名
	private Integer app_version_number;//应用版本号
	private String dev_uuid;//设备唯一标示
	private String dev_mac_addr;//设备Mac地址
	private String dev_manufacturer;//设备制造商
	private String dev_model;//设备型号
	private String activated_at;//激活时间
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getOs_name() {
		return os_name;
	}
	public void setOs_name(String os_name) {
		this.os_name = os_name;
	}
	public String getOs_lang() {
		return os_lang;
	}
	public void setOs_lang(String os_lang) {
		this.os_lang = os_lang;
	}
	public String getOs_ver() {
		return os_ver;
	}
	public void setOs_ver(String os_ver) {
		this.os_ver = os_ver;
	}
	public String getApp_bundle_id() {
		return app_bundle_id;
	}
	public void setApp_bundle_id(String app_bundle_id) {
		this.app_bundle_id = app_bundle_id;
	}
	public Integer getApp_version_number() {
		return app_version_number;
	}
	public void setApp_version_number(Integer app_version_number) {
		this.app_version_number = app_version_number;
	}
	public String getDev_uuid() {
		return dev_uuid;
	}
	public void setDev_uuid(String dev_uuid) {
		this.dev_uuid = dev_uuid;
	}
	public String getDev_mac_addr() {
		return dev_mac_addr;
	}
	public void setDev_mac_addr(String dev_mac_addr) {
		this.dev_mac_addr = dev_mac_addr;
	}
	public String getDev_manufacturer() {
		return dev_manufacturer;
	}
	public void setDev_manufacturer(String dev_manufacturer) {
		this.dev_manufacturer = dev_manufacturer;
	}
	public String getDev_model() {
		return dev_model;
	}
	public void setDev_model(String dev_model) {
		this.dev_model = dev_model;
	}
	public String getActivated_at() {
		return activated_at;
	}
	public void setActivated_at(String activated_at) {
		this.activated_at = activated_at;
	}
	
}
