package com.ws.entity;
/**
 * @description只是为了封装经度和纬度便于转成json
 * @author  wusong
 * @time 2016-9-16 下午3:59:53
 */
public class Location {
	private Double lon;//经度
	private Double lat;//纬度
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	
}
