package com.ws.entity;

public class Speed {
	private Integer sid;//主键
	private Integer uid;//用户ID
	private Integer ping;//网络延迟
	private Integer download;//下载速度
	private Integer upload;//上传速度
	private Integer bytesForUpload;//上传字节数
	private Integer bytesForDownload;//下载字节数
	private Integer networkType;//网络类型：0 unknown，1 cell ，2 WiFi
	private Double lon;//经度
	private Double lat;//纬度
	private String date;//日期时间
	private String crarrierName;//运营商
	private String wifiSSID;//WiFi名称
	private String externalIP;//外网IP
	private String internalIP;//内网IP
	private String remark;//备注
	private Integer rank;//下载速度排名
	private Location location;
	public Integer getSid() {
		return sid;
	}
	public void setSid(Integer sid) {
		this.sid = sid;
	}
	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public Integer getPing() {
		return ping;
	}
	public void setPing(Integer ping) {
		this.ping = ping;
	}
	public Integer getDownload() {
		return download;
	}
	public void setDownload(Integer download) {
		this.download = download;
	}
	public Integer getUpload() {
		return upload;
	}
	public void setUpload(Integer upload) {
		this.upload = upload;
	}
	public Integer getBytesForUpload() {
		return bytesForUpload;
	}
	public void setBytesForUpload(Integer bytesForUpload) {
		this.bytesForUpload = bytesForUpload;
	}
	public Integer getBytesForDownload() {
		return bytesForDownload;
	}
	public void setBytesForDownload(Integer bytesForDownload) {
		this.bytesForDownload = bytesForDownload;
	}

	public Integer getNetworkType() {
		return networkType;
	}
	public void setNetworkType(Integer networkType) {
		this.networkType = networkType;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCrarrierName() {
		return crarrierName;
	}
	public void setCrarrierName(String crarrierName) {
		this.crarrierName = crarrierName;
	}
	public String getWifiSSID() {
		return wifiSSID;
	}
	public void setWifiSSID(String wifiSSID) {
		this.wifiSSID = wifiSSID;
	}
	public String getExternalIP() {
		return externalIP;
	}
	public void setExternalIP(String externalIP) {
		this.externalIP = externalIP;
	}
	public String getInternalIP() {
		return internalIP;
	}
	public void setInternalIP(String internalIP) {
		this.internalIP = internalIP;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
}
