package com.ws.tool;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author wusong
 * 
 */
public class BaseAction extends ActionSupport{
	    // 
	public Map<String, Object> getRequestMap() {
		// 
		return (Map) ActionContext.getContext().get("request");
	}
	
	public HttpServletRequest getRequest(){
		HttpServletRequest request = ServletActionContext.getRequest();
		return request;
	}

	//
	public Map<String, Object> getSession() {
		return ActionContext.getContext().getSession();
	}

	// 
	public Map<String, Object> getApplication() {
		return (Map) ActionContext.getContext().getApplication();
	}

	// 
	public HttpServletResponse getResponse() {
		// 
		HttpServletResponse response = ServletActionContext.getResponse();
		// 
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("utf-8");
		return response;
	}
	
	public String getParamValue(String param){
	    return getRequest().getParameter(param);
	}
	public String[] getparamValues(String param){
	    return getRequest().getParameterValues(param);
	}
	
	//直接获取页面或者地址传入的数据流并且转换成json字符串
	public String inputStreamToJson(){
		try {
			ServletInputStream servletInputStream = getRequest().getInputStream();
			return IOUtils.toString(servletInputStream);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
