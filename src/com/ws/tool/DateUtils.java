package com.ws.tool;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	/**
	 * @description 获取当前系统时间字符串 yyyy-MM-dd hh:mm:ss
	 * @auther wusong
	 * @time 2016-3-13 下午6:10:42
	 */
	public static String getCurrDateStr(){
		Format format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return format.format(new Date());
	}
	/**
	 * @description 获取系统当前的日期字符串  yyyy-MM-dd
	 * @auther wusong
	 * @time 2016-3-13 下午6:12:18
	 */
	public static String getNowDate(){
		Format format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(new Date());
	}
	
	/**
	 * @description 
	 * @auther wusong
	 * @time 2016-3-13 下午6:12:56
	 */
	
	public static void main(String[] args) {
		System.out.println(getNowDate());
	}
}
