package com.ws.tool;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	private static Configuration conf;  
	  
    private static SessionFactory factory;  
    static {  
        conf = new Configuration();  
        conf.configure();  
        factory = conf.buildSessionFactory();  
    }  
  
    public static Session getSession() {  
        return factory.openSession(); 
    }  
  
    public static SessionFactory getSessionFactory() {  
        return factory;  
    }  
  
    public static void main(String[] args) {  
        Session session = HibernateUtils.getSession();  
        System.out.println(session);  
    }  
}
