package com.ws.web.action;

import java.io.IOException;

import org.apache.struts2.ServletActionContext;

import com.ws.biz.DeviceService;
import com.ws.entity.Device;
import com.ws.tool.BaseAction;
import com.ws.tool.JSONUtils;

/**
 * @description设备激活
 * @author  wusong
 * @time 2016-3-13 下午4:08:54
 */
public class DeviceAction extends BaseAction {
	private DeviceService deviceService;
	
	public DeviceService getDeviceService() {
		return deviceService;
	}

	public void setDeviceService(DeviceService deviceService) {
		this.deviceService = deviceService;
	}
	/**
	 * @description 用户设备激活（保存用户的设备记录，返回用户ID和激活时间，token） 
	 * @auther wusong
	 * @time 2016-3-13 下午8:48:47
	 */
	public void activateDevice(){
		String speedJson = this.inputStreamToJson();
		Device device = JSONUtils.json2Bean(speedJson, Device.class);
		String responseJson = deviceService.activateDevice(device);
		try {
			JSONUtils.toJson(ServletActionContext.getResponse(),responseJson);
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
