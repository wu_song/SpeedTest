package com.ws.web.action;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.ws.biz.SpeedService;
import com.ws.entity.Speed;
import com.ws.tool.BaseAction;
import com.ws.tool.JSONUtils;

public class SpeedAction extends BaseAction{
	private SpeedService speedService;
	
	public void saveSpeed(){
	    String speedJson = this.inputStreamToJson();
	    Speed speed = JSONUtils.json2Bean(speedJson, Speed.class);
		String returnJson = speedService.saveSpeed(speed);
		try {
			JSONUtils.toJson(ServletActionContext.getResponse(), returnJson);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * @description 获取传入用户附近排名前20名的速度信息
	 * @auther wusong
	 * @time 2016-9-16 下午4:00:49
	 */
	public void getNearByRank(){
		 String speedJson = this.inputStreamToJson();
		 Speed speed = JSONUtils.json2Bean(speedJson, Speed.class);
		 List<Speed> speedList = speedService.getNearByRank(speed);   
		 try {
			JSONUtils.toJson(ServletActionContext.getResponse(), speedList);
		 } catch (IOException e) {
			e.printStackTrace();
		 }
	}
	/**
	 * @description 获取速度排名的记录
	 * @auther wusong
	 * @time 2016-9-16 下午4:00:30
	 */
	public void getSpeedRank(){
		String paramsJson = this.inputStreamToJson();
		Map paramMap = JSONUtils.json2Map(paramsJson);
		if(paramMap.get("limit") != null && paramMap.get("page") != null){
			String currPage = (String) paramMap.get("limit");
			String rows = (String) paramMap.get("page");
			List<Speed> speedList = speedService.getSpeedRankList(Integer.parseInt(rows), Integer.parseInt(currPage));
			try {
				JSONUtils.toJson(ServletActionContext.getResponse(), speedList);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public SpeedService getSpeedService() {
        return speedService;
    }
    public void setSpeedService(SpeedService speedService) {
        this.speedService = speedService;
    }
}
