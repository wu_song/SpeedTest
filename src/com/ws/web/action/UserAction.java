package com.ws.web.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;


import com.ws.biz.UserHander;
import com.ws.entity.User;
import com.ws.tool.BaseAction;
import com.ws.tool.JSONUtils;


/**
 * @author wusong
 * @Email wu_song4232@126.com
 */
public class UserAction extends BaseAction{
	private User user;
	private UserHander userHander;
	
	private String name;
	private String pass;
	
	public String execute(){
		Map<String, Object> map=new HashMap<String, Object>();
		String status=null;
		try {
			if(!StringUtils.isEmpty(name) && !StringUtils.isEmpty(pass)) {
				User user=new User();
				user.setName(name);
				user.setPassword(pass);
				List<User> list=userHander.getUserDAO().findByExample(user);
				if (list.size()>0) {
					status="1";
					map.put("users", list);
				}else {
					status="null";
				}
			}else {
				status="0";
			}
			map.put("status", status);
			JSONUtils.toJson(ServletActionContext.getResponse(), map);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	public void listUser(){
		User user=new User();
		user.setName("wusong");
		user.setPassword("wusong");
		try {
			List<User> list=userHander.getUserDAO().findByExample(user);
			JSONUtils.toJson(getResponse(), user);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public UserHander getUserHander() {
		return userHander;
	}
	public void setUserHander(UserHander userHander) {
		this.userHander = userHander;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
}
