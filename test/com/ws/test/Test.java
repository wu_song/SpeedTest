package com.ws.test;

public class Test {
	int value;
	private static Test instance;
	
	private Test(){
		System.out.println("构造器...");
	}
	
	public static Test getInstance(){
		if(instance == null){
			instance =  new Test();
		}
		return instance;
	}
	
	public int getValue(){
		return value;
	}
	public void setValue(int value){
		this.value = value;
	}
	/** 
	 * @description 
	 * @auther wusong
	 * @time 2016-3-12 下午3:55:04
	 */
	public static void main(String[] args) {
		Test test1 = Test.getInstance();
		Test test2 = Test.getInstance();
		System.out.println(test1 == test2);
	}

}
