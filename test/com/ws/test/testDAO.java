package com.ws.test;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ws.dao.UserDAO;
import com.ws.entity.User;



public class testDAO {
	ApplicationContext ctx;
	@Before
	public void setUp() throws Exception {
		ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		UserDAO userDAO = UserDAO.getFromApplicationContext(ctx);
		User user = new User();
		user.setName("wusong");
		user.setPassword("123");
		List<User> list = userDAO.findByExample(user);
		System.out.println(list.get(0).getNum());
	}

	@Test
	public void test2() {
		UserDAO userDAO = UserDAO.getFromApplicationContext(ctx);
		User user = new User();
		user.setName("wusong");
		user.setPassword("123");
		List<User> list = userDAO.findByExample(user);
		System.out.println(toJson(list.get(0)));
	}
	
	public String toJson(Object obj) {
	    return JSONObject.fromObject(obj).toString();
	}
	 
	public String toJsonArray(Object array) {
	    return JSONArray.fromObject(array).toString();
	}
}
